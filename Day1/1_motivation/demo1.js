console.log("Demo One....");

function Employee(name) {
    this._name = name;

    this.getName = function() {
        return this._name.toUpperCase();
    }

    this.setName = function(name) {
        this._name = name;
    }
}

var employeeOne = new Employee("John");
console.log(employeeOne.getName());
employeeOne.setName("Ramakant");
console.log(employeeOne.getName());

var employeeTwo = new Employee("Subodh");
console.log(employeeTwo.getName());
employeeTwo.setName("Abhijeet");
console.log(employeeTwo.getName());
