console.log("Demo Two....");

const Employee = (function() {
    function Employee(name) {
        this._name = name;
    }
    
    Employee.prototype.getName = function() {
        return this._name.toUpperCase();
    }
    
    Employee.prototype.setName = function(name) {
        this._name = name;
    }

    return Employee
})();

var employeeOne = new Employee("John");
console.log(employeeOne.getName());
employeeOne.setName("Ramakant");
console.log(employeeOne.getName());

var employeeTwo = new Employee("Subodh");
console.log(employeeTwo.getName());
employeeTwo.setName("Abhijeet");
console.log(employeeTwo.getName());
