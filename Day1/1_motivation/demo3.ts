console.log("Demo Three....");

class Employee {
    private _name: string;

    constructor(name:string) {
        this._name = name;
    }

    getName(): string {
        return this._name.toUpperCase();
    }

    setName(name: string): void {
        this._name = name;
    }
}

var employeeOne = new Employee("John");
console.log(employeeOne.getName());
employeeOne.setName("Ramakant");
console.log(employeeOne.getName());

var employeeTwo = new Employee("Subodh");
console.log(employeeTwo.getName());
employeeTwo.setName("Abhijeet");
console.log(employeeTwo.getName());
