// const area = function (rect: { h: number, w: number }) {
//     return rect.w * rect.h;
// }

// let shape1 = { h: 20, w: 10 };
// console.log(area(shape1));

// let shape2 = { h: 20, w: 10, d: 30 };
// console.log(area(shape2));

// -------------------------------
// type Shape = { h: number, w: number };      // Type Alias

// const area = function (rect: Shape) {
//     return rect.w * rect.h;
// }

// let shape1: Shape = { h: 20, w: 10 };
// console.log(area(shape1));

// let shape2: Shape = { h: 20, w: 10, d: 30 };
// console.log(area(shape2));

// let shape3: Shape = { h: 20 };
// console.log(area(shape3));

// -------------------------------
// type Shape = { h: number, w?: number };      // Type Alias

// const area = function (rect: Shape) {
//     rect.w = rect.w || rect.h;
//     return rect.w * rect.h;
// }

// let shape1: Shape = { h: 20, w: 10 };
// console.log(area(shape1));

// let shape2: Shape = { h: 20, w: 10, d: 30 };
// console.log(area(shape2));

// let shape3: Shape = { h: 20 };
// console.log(area(shape3));

// // -------------------------------
// interface IShape 
// { 
//     h: number; 
//     w?: number; 
// }

// const area = function (rect: IShape) {
//     rect.w = rect.w || rect.h;
//     return rect.w * rect.h;
// }

// let shape1: IShape = { h: 20, w: 10 };
// console.log(area(shape1));

// let shape2: IShape = { h: 20, w: 10, d: 30 };
// console.log(area(shape2));

// let shape3: IShape = { h: 20 };
// console.log(area(shape3));

// // ---------------------------------------- Merging
// // type TShape = { 
// //     h: number 
// // };

// // // Error: Duplicate identifier 'TShape'
// // type TShape = { 
// //     w: number 
// // };

// interface IShape {
//     h: number;
// };

// interface IShape {
//     w: number;
// };

// let s3: IShape = {};

interface ICustomer {
    doShopping(): string;
}

interface IEmployee {
    doWork(): string;
}

// var person1: ICustomer | IEmployee = {
//     doShopping: function () { 
//         return "Shopping done!";
//     }
// }

type ICustomerOrIEmployee = ICustomer | IEmployee;      // Union (TypeGuard)

var person1: ICustomerOrIEmployee = {
    doShopping: function () { 
        return "Shopping done!";
    }
}

type ICustomerAndIEmployee = ICustomer & IEmployee;      // Intersection

var person2: ICustomerAndIEmployee = {
    doShopping: function () {
        return "Shopping done!";
    },
    doWork: function () {
        return "Work done!";
    }
}