// Global Scope
// File Scope
// Namespace Scope
// Function Scope
// Block Scope

// var i = 20;
// console.log(i);

// namespace scopes {
//     var i = 20;
//     console.log(i);
// }

// namespace scopes {
//     export var i = 20;
// }

// namespace usage {
//     console.log(scopes.i);
// }

// var i = 100;
// console.log("Before Loop, i is: ", i);

// for (var i = 0; i < 5; i++) {
//     console.log("Inside Loop, i is: ", i);
// }

// console.log("After Loop, i is: ", i);

var i = 100;
console.log("Before Loop, i is: ", i);

for (let i = 0; i < 5; i++) {
    console.log("Inside Loop, i is: ", i);
}

console.log("After Loop, i is: ", i);