let data1: any = 'this is a string';

console.log((<string>data1).toUpperCase());
console.log((data1 as string).toUpperCase());

let s1 = data1.length;

let s2 = (<string>data1).length;

let s3 = (data1 as string).length;

// Wrong Assertion will cost you runtime error
console.log((data1 as number).toFixed());
