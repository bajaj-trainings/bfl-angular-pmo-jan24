// function hello() {
//     console.log("Hello World!");
// }

// hello();
// hello("Manish");

// function hi(name: string) {
//     console.log("Hi, ", name);
// }

// hi();
// hi("Manish");

// --------------------------------------

// 1. Fn Declaration
function add0(x: number, y: number) {
    return (x + y).toString();
}

function add1(x: number, y: number): number {
    return x + y;
}

// 2. Fn Expression
let add2 = function (x: number, y: number): number {
    return x + y;
}

// let add3;
// let add3: () => void;
let add3: (a: number, b: number) => number;
add3 = function (x: number, y: number): number {
    return x + y;
}

let add4: (a: number, b: number) => number;
add4 = function (x, y) {
    return x + y;
}

// Lambdas - Multiline
let add5: (a: number, b: number) => number;
add5 = (x, y) => {
    return x + y;
}

// Lambdas - Singleline
let add6: (a: number, b: number) => number;
add6 = (x, y) => x + y;


console.log(add1(20, 30));
console.log(add2(20, 30));
console.log(add3(20, 30));
console.log(add4(20, 30));
console.log(add5(20, 30));
console.log(add6(20, 30));