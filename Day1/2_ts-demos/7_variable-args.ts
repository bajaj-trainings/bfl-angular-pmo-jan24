// REST Parameter
function Average(...args: number[]) {
    if (args.length > 0) {
        let sum = args.reduce((acc, n) => acc + n);
        return sum / args.length;
    } else {
        return 0;
    }
}

console.log(Average());
console.log(Average(1));
console.log(Average(1, 2));
console.log(Average(1, 2, 3, 4, 5));
console.log(Average(1, 2, 3, 4, 5, 6, 7, 8, 9));

let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
console.log(Average(...numbers));       // Spread

function M1(...args: (number | string)[]) {

}

M1(10, 20, 30, 40, 50);
M1("Hello", "Hi");
M1(10, "Hi");
M1("Hi", 10);

var arr1: (number | string)[];      // TypeGuard
arr1 = [10, 20, 30, 40, 50];
arr1 = ["Hello", "Hi"];
arr1 = [10, "Hi"];
arr1 = ["Hi", 10];

var arr2: [number, string];         // Tuple
// arr2 = [10, 20, 30, 40, 50];
// arr2 = ["Hello", "Hi"];
arr2 = [10, "Hi"];
// arr2 = ["Hi", 10];
// arr2 = [10, "Hi", 20];