function hello() {
    console.log("Hello World!");
}

hello();
hello("Manish");

function hi(name) {
    console.log("Hi, ", name);
}

hi();
hi("Manish");
