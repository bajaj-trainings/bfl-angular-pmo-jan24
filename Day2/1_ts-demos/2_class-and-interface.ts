// interface IPerson {
//     name: string;
//     age: number;
//     greet(message: string): string;
// }

// // let p1: IPerson = { 
// //     name: "NA", 
// //     age: 0, 
// //     greet: function (message: string) {
// //         return message + " " + this.name;
// //     }
// // };

// // let p2: IPerson = { 
// //     name: "NA", 
// //     age: 0, 
// //     greet: function (message: string) {
// //         return message + " " + this.name;
// //     }
// // };

// class Person implements IPerson {
//     constructor(public name: string, public age: number) { }

//     greet(message: string): string {
//         return message + " " + this.name;
//     }
// }

// let p1: IPerson = new Person("John", 40);
// let p2: IPerson = new Person("Jane", 30);

// console.log(p1.greet("Hello"));
// console.log(p2.greet("Hello"));

// // ------------------------------------------------- Multiple Interface Implementation

// interface IPerson {
//     name: string;
//     age: number;
//     greet(message: string): string;
// }

// interface IEmployee {
//     doWork(): string;
// }

// class Person implements IPerson, IEmployee {
//     constructor(public name: string, public age: number) { }

//     greet(message: string): string {
//         return message + " " + this.name;
//     }

//     doWork(): string {
//         return "I am learning TypeScript";
//     }
// }

// let p1: Person = new Person("John", 40);
// console.log(p1.greet("Hello"));
// console.log(p1.doWork());

// ------------------------------------------------- Multiple Interface Implementations
// interface IPerson {
//     name: string;
//     age: number;
//     greet(message: string): string;
// }

// interface IEmployee {
//     doWork(): string;
// }

// interface ICustomer {
//     doShopping(): string;
// }

// class Person implements IPerson, IEmployee, ICustomer {
//     constructor(public name: string, public age: number) { }
        
//     greet(message: string): string {
//         return message + " " + this.name;
//     }
    
//     doWork(): string {
//         return "I am learning TypeScript";
//     }

//     doShopping(): string {
//         return "I am at the mall";
//     }
// }

// let p1: Person = new Person("John", 40);
// console.log(p1.greet("Hello"));
// console.log(p1.doWork());
// console.log(p1.doShopping());

// let p2: IPerson = new Person("Jane", 30);
// console.log(p2.greet("Hello"));

// let p3: IEmployee = new Person("Jane", 30);
// console.log(p3.doWork());

// let p4: ICustomer = new Person("Jane", 30);
// console.log(p4.doShopping());

// ------------------------------------------------- Interface can extend other Interface
interface IPerson {
    name: string;
    age: number;
    greet(message: string): string;
}

interface IEmployee extends IPerson {
    doWork(): string;
}

interface ICustomer extends IPerson {
    doShopping(): string;
}

class Person implements IPerson, IEmployee, ICustomer {
    constructor(public name: string, public age: number) { }
        
    greet(message: string): string {
        return message + " " + this.name;
    }
    
    doWork(): string {
        return "I am learning TypeScript";
    }

    doShopping(): string {
        return "I am at the mall";
    }
}

let p1: Person = new Person("John", 40);
console.log(p1.greet("Hello"));
console.log(p1.doWork());
console.log(p1.doShopping());

let p2: IPerson = new Person("Jane", 30);
console.log(p2.greet("Hello"));

let p3: IEmployee = new Person("Jane", 30);
console.log(p3.greet("Hi"));
console.log(p3.doWork());

let p4: ICustomer = new Person("Jane", 30);
console.log(p4.greet("Hey"));
console.log(p4.doShopping());