// // Eager Executed
// function idNormal() {
//     console.log("Id Normal Function Executed...");
// }

// idNormal();

// // Lazy (On Demand) Executed
// function* idGenerator() {
//     console.log("Id Generator Function Executed...");
// }

// // idGenerator();
// let gObject = idGenerator();
// gObject.next();

// ---------------------------------------------------

// function* idGenerator() {
//     console.log("Id Generator Function Execution Started....");
//     yield 1;
//     yield 2;
//     yield 3;
//     yield 4;
//     console.log("Id Generator Function Execution Completed....");
// }

// // let gObject = idGenerator();
// // console.log(gObject.next());
// // console.log(gObject.next());
// // console.log(gObject.next());
// // console.log(gObject.next());

// for (const idData of idGenerator()) {
//     console.log(idData);
// }

class Queue<T> {
    private _data: T[] = [];
    private _i: number = 0;

    push(item: T) {
        this._data.push(item);
    }

    pop(): T | undefined {
        return this._data.shift();
    }

    *[Symbol.iterator]() {
        for (let i = 0; i < this._data.length; i++) {
            yield this._data[i]
        }
    }
}

type Order = {
    id: number,
    description: string
}

var ordersQ = new Queue<Order>();
ordersQ.push({ id: 1, description: "Order One" });
ordersQ.push({ id: 2, description: "Order Two" });
ordersQ.push({ id: 3, description: "Order Three" });

for (const item of ordersQ) {
    console.log(item);
}