import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BflLibComponent } from './bfl-lib.component';

describe('BflLibComponent', () => {
  let component: BflLibComponent;
  let fixture: ComponentFixture<BflLibComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BflLibComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BflLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
