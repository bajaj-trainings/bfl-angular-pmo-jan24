import { TestBed } from '@angular/core/testing';

import { BflLibService } from './bfl-lib.service';

describe('BflLibService', () => {
  let service: BflLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BflLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
