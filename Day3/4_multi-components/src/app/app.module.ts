// import { NgModule } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
// import { ComponentOneComponent } from './components/component-one/component-one.component';
// import { ComponentTwoComponent } from './components/component-two/component-two.component';

// @NgModule({
//   declarations: [
//     ComponentOneComponent,
//     ComponentTwoComponent
//   ],
//   imports: [
//     BrowserModule
//   ],
//   providers: [],
//   bootstrap: [
//     ComponentOneComponent,
//     ComponentTwoComponent
//   ]
// })
// export class AppModule { }

// ----------------------------- Using Root

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentOneComponent } from './components/component-one/component-one.component';
import { ComponentTwoComponent } from './components/component-two/component-two.component';
import { RootComponent } from './components/root/root.component';

@NgModule({
  declarations: [
    ComponentOneComponent,
    ComponentTwoComponent,
    RootComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [
    RootComponent
  ]
})
export class AppModule { }
