import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompTwoComponent } from './components/comp-two/comp-two.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    CompTwoComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    CompTwoComponent
  ]
})
export class MtwoModule { }
