import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RootComponent } from './components/root/root.component';
import { CompOneComponent } from './components/component-one/component-one.component';
import { CompTwoComponent } from './components/component-two/component-two.component';

@NgModule({
  declarations: [
    CompOneComponent,
    CompTwoComponent,
    RootComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [
    RootComponent
  ]
})
export class AppModule { }
