import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RootComponent } from './components/root/root.component';
import { AssignTwoComponent } from './components/assign-two/assign-two.component';
import { FormsModule } from '@angular/forms';
import { ListComponent } from './components/list/list.component';
import { HighlightDirective } from './directives/highlight.directive';

@NgModule({
  declarations: [
    RootComponent,
    AssignTwoComponent,
    ListComponent,
    HighlightDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [
    RootComponent
  ]
})
export class AppModule { }
