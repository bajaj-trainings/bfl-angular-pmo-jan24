import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RootComponent } from './components/root/root.component';
import { FormsModule } from '@angular/forms';
import { ListComponent } from './components/list/list.component';
import { CounterComponent } from './components/counter/counter.component';

@NgModule({
  declarations: [
    RootComponent,
    ListComponent,
    CounterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [
    RootComponent
  ]
})
export class AppModule { }
