import { Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CounterComponent } from '../counter/counter.component';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html'
})
export class RootComponent {
  pList: Array<string>;
  message: string;

  constructor() {
    this.pList = ['Manish', 'Ramesh', 'Suresh', 'Rajesh', 'Dinesh', 'Ganesh'];
    this.message = "";
  }

  @ViewChild("c1", { static: true })
  counter?: CounterComponent;

  @ViewChild("c2", { static: true })
  counter2?: CounterComponent;

  @ViewChildren(CounterComponent)
  counters?: QueryList<CounterComponent>;

  p2_reset(c1: CounterComponent) {
    // Logic
    c1.reset();
  }

  p3_reset() {
    this.counter?.reset();
    this.counter2?.reset();
  }

  reset_all() {
    if (this.counters) {
      this.counters.forEach(c => c.reset());
    }
  }

  updateMessage(flag: boolean) {
    if (flag)
      this.message = "Max Click Reached, please Reset to restart";
    else
      this.message = "";
  }
}
