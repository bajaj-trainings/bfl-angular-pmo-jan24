import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styles: [
  ]
})
export class ListComponent {
  @Input() personList?: Array<string>;
  selected: string = '';
  by: string = '';

  select(person: string, e: Event) {
    this.selected = person;
    e.preventDefault();

  }
}
