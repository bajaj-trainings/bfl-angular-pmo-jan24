import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RootComponent } from './components/root/root.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReactiveFormComponent } from './components/reactive-form/reactive-form.component';
import { ValidationFormComponent } from './components/validation-form/validation-form.component';
import { EnterKeyFocusDirective } from './directives/enter-key-focus.directive';
import { HighlightDropdownDirective } from './directives/highlight-dropdown.directive';
import { DynamicPlaceholderDirective } from './directives/dynamic-placeholder.directive';

@NgModule({
  declarations: [
    RootComponent,
    ReactiveFormComponent,
    ValidationFormComponent,
    EnterKeyFocusDirective,
    HighlightDropdownDirective,
    DynamicPlaceholderDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [
    RootComponent
  ]
})
export class AppModule { }
