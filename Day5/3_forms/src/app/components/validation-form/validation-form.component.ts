import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'validation-form',
  templateUrl: './validation-form.component.html',
  styleUrls: ['./validation-form.component.css']
})
export class ValidationFormComponent {
  regForm: FormGroup;

  countries = [
    { 'id': "", 'name': 'Select Country' },
    { 'id': 1, 'name': 'India' },
    { 'id': 2, 'name': 'USA' },
    { 'id': 3, 'name': 'UK' }
  ];
  
  constructor(private frmBuilder: FormBuilder) {
    this.regForm = this.frmBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(6)
      ])],
      gender: ['', Validators.required],
      age: ['', [
        Validators.required
      ]],
      address: this.frmBuilder.group({
        country: ['', Validators.required],
        city: ['', Validators.required],
        zip: ['', Validators.required],
        fulladdress: ['', Validators.required],
      }),
      acceptTerms: ['', Validators.requiredTrue],
    });
  }

  logForm() {
    if (this.regForm.valid) {
      console.log(this.regForm.value);
      // Call API to save data
    } else {
      console.error('Form is invalid');
      this.regForm.markAllAsTouched();
    }
  }

  get frm() { return this.regForm.controls; }
  get address() { return (this.regForm.controls['address'] as FormGroup).controls; }
}
