import { Directive, ElementRef, HostListener, Input, Renderer2, OnInit } from '@angular/core';

@Directive({
  selector: '[dynamicPlaceholder]'
})
export class DynamicPlaceholderDirective implements OnInit {
  @Input('appDynamicDatePlaceholder') fullPlaceholder: string = 'DD/MM/YYYY'; // Default value
  private overlayElement?: HTMLElement;

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  ngOnInit(): void {
    this.createOverlayElement();
  }

  createOverlayElement() {
    // Create a span to act as the overlay element
    this.overlayElement = this.renderer.createElement('span');
    this.renderer.addClass(this.overlayElement, 'placeholder-overlay');
    this.renderer.setProperty(this.overlayElement, 'innerHTML', this.fullPlaceholder);

    // Position the overlay relative to the input
    const parent = this.renderer.parentNode(this.el.nativeElement);
    this.renderer.setStyle(parent, 'position', 'relative');
    this.renderer.appendChild(parent, this.overlayElement);

    // Position the overlay on top of the input field
    this.renderer.setStyle(this.overlayElement, 'position', 'absolute');
    this.renderer.setStyle(this.overlayElement, 'left', '0');
    this.renderer.setStyle(this.overlayElement, 'top', '0');
    this.renderer.setStyle(this.overlayElement, 'height', '100%');
    this.renderer.setStyle(this.overlayElement, 'lineHeight', this.el.nativeElement.offsetHeight + 'px');
    this.renderer.setStyle(this.overlayElement, 'color', 'grey');
    this.renderer.setStyle(this.overlayElement, 'pointerEvents', 'none'); // Allows click through the overlay
  }

  @HostListener('input', ['$event.target.value']) onInput(value: string) {
    // Logic to update the overlay's innerHTML based on the input length
    let displayPlaceholder = this.fullPlaceholder;
    for (let i = 0; i < value.length; i++) {
      if (i < displayPlaceholder.length) {
        displayPlaceholder = ' ' + displayPlaceholder.slice(1);
      }
    }
    this.renderer.setProperty(this.overlayElement, 'innerHTML', displayPlaceholder);
  }

  @HostListener('blur') onBlur() {
    if (!this.el.nativeElement.value) {
      this.renderer.setProperty(this.overlayElement, 'innerHTML', this.fullPlaceholder);
    }
  }

  @HostListener('focus') onFocus() {
    this.onInput(this.el.nativeElement.value);
  }
}
