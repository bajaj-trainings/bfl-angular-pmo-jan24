import { Directive, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[highlightDropdown]'
})
export class HighlightDropdownDirective {
  @HostBinding('class.highlight') highlight: boolean = false;

  constructor() { }

  @HostListener('change') onChange() {
    this.highlight = true;
    setTimeout(() => this.highlight = false, 2000);
  }
}
