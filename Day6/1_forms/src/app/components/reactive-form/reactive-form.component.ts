import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'reactive-form',
  templateUrl: './reactive-form.component.html',
})
export class ReactiveFormComponent {
  regForm: FormGroup;

  constructor(private frmBuilder: FormBuilder) {
    this.regForm = this.frmBuilder.group({
      firstname: '',
      lastname: '',
      address: this.frmBuilder.group({
        city: '',
        zip: ''
      })
    });
  }

  logForm() {
    console.log(this.regForm.value);
    // Call API to save data
  }
}
