import { Component, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CustomValidators } from 'src/app/utilities/custom-validators';

@Component({
  selector: 'validation-form',
  templateUrl: './validation-form.component.html',
  styleUrls: ['./validation-form.component.css']
})
export class ValidationFormComponent {
  regForm: FormGroup;

  countries = [
    { 'id': "", 'name': 'Select Country' },
    { 'id': 1, 'name': 'India' },
    { 'id': 2, 'name': 'USA' },
    { 'id': 3, 'name': 'UK' }
  ];

  minAge = 20;
  maxAge = 60;
  
  constructor(private frmBuilder: FormBuilder, private el: ElementRef) {
    this.regForm = this.frmBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(6)
      ])],
      username: ['', {
        validators: [Validators.required],
        asyncValidators: [CustomValidators.usernameValidator()]
      }],
      gender: ['', Validators.required],
      age: ['', [
        Validators.required,
        // CustomValidators.ageRange
        CustomValidators.ageRange(this.minAge, this.maxAge)
      ]],
      address: this.frmBuilder.group({
        country: ['', Validators.required],
        city: ['', Validators.required],
        zip: ['', Validators.required],
        fulladdress: ['', Validators.required],
      }),
      acceptTerms: ['', Validators.requiredTrue],
    });
  }

  logForm() {
    if (this.regForm.valid) {
      console.log(this.regForm.value);
      // Call API to save data
    } else {
      console.error('Form is invalid');
      this.regForm.markAllAsTouched();
      this.focusFirstInvalidControl();
    }
  }

  private focusFirstInvalidControl() {
    for (const key of Object.keys(this.regForm.controls)) {
      if (this.regForm.controls[key].invalid) {
        const invalidControl = this.el.nativeElement.querySelector('[formControlName="' + key + '"]');
        if (invalidControl) {
          invalidControl.focus();
          break; // Stop after focusing the first invalid control
        }
      }
    }
  }

  get frm() { return this.regForm.controls; }
  get address() { return (this.regForm.controls['address'] as FormGroup).controls; }
}
