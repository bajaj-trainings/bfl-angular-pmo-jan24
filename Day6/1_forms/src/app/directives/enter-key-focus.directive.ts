import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[enterKeyFocus]'
})
export class EnterKeyFocusDirective {
  constructor(private el: ElementRef) { }

  @HostListener('keydown', ['$event'])
  onKeydown(event: KeyboardEvent) {
    if (event.key === 'Enter') {
      event.preventDefault();
      const elements = this.getFocusableElements();
      const index = elements.findIndex(el => el === document.activeElement);
      if (index > -1 && index < elements.length - 1) {
        elements[index + 1].focus();
      }
    }
  }

  private getFocusableElements(): HTMLElement[] {
    const selectors = 'input, button, select, textarea, a[href]';
    const elements = this.el.nativeElement.querySelectorAll(selectors);

    return Array.from(elements).filter((el): el is HTMLElement => {
      // Check if el is an instance of a specific element type that has the disabled property
      if (el instanceof HTMLInputElement || el instanceof HTMLSelectElement || el instanceof HTMLButtonElement || el instanceof HTMLTextAreaElement) {
        // Now it's safe to access the disabled property
        return !el.disabled && el.tabIndex >= 0;
      } else if (el instanceof HTMLElement) {
        // For other HTMLElements that do not have a disabled property, just check tabIndex
        return el.tabIndex >= 0;
      }
      return false;
    });
  }
}
