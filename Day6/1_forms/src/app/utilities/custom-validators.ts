// import { AbstractControl, ValidationErrors } from "@angular/forms";

// export class CustomValidators {
//     static ageRange(control: AbstractControl): ValidationErrors | null {
//         if (control.value !== '' && (isNaN(control.value) || control.value < 18 || control.value > 60)) {
//             return { 'ageRange': true };
//         } else {
//             return null;
//         }
//     }
// }

import { AbstractControl, AsyncValidatorFn, ValidationErrors, ValidatorFn } from "@angular/forms";
import { Observable, delay, map, of } from "rxjs";

export class CustomValidators {
    static ageRange(min = 18, max = 60): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            if (control.value !== '' && (isNaN(control.value) || control.value < min || control.value > max)) {
                return { 'ageRange': true };
            } else {
                return null;
            }
        }
    }

    private static takenUsernames = [
        'manish',
        'manishs',
        'manishsharma',
        'manish.sharma',
        'manish_sharma'
    ];

    private static isUsernameTaken(username: string): Observable<boolean> {
        // HTTP API Call to check Username Status
        return of(CustomValidators.takenUsernames.includes(username)).pipe(delay(1000));
    }

    static usernameValidator(): AsyncValidatorFn {
        return (control: AbstractControl): Observable<ValidationErrors | null> => { 
            return CustomValidators.isUsernameTaken(control.value).pipe(
                map(isTaken => {
                    return isTaken ? {'usernameExists': true} : null
                })
            );
        }
    }
}