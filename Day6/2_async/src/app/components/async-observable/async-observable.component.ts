import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription, interval, map } from 'rxjs';

@Component({
  selector: 'async-observable',
  template: `
    <h2 class="text-info">Observable & Async Pipe</h2>  
    <h2>Result: {{observableData}}</h2>
    <h2>Observable: {{observableInstance | async}}</h2>
  `
})
export class AsyncObservableComponent implements OnInit, OnDestroy {
  observableData?: number;
  observableInstance?: Observable<number>;

  sub?: Subscription;

  ngOnInit(): void {
    this.sub = this.getObservable().subscribe((data: number) => {
      this.observableData = data;
    });

    this.observableInstance = this.getObservable();
  }

  getObservable(): Observable<number> {
    return interval(4000).pipe(map((_: any) => Math.random()));
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }
}
