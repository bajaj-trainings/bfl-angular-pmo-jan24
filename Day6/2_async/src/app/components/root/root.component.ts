import { Component } from '@angular/core';
import { BehaviorSubject, EMPTY, Observable, ReplaySubject, Subject, concat, delay, filter, from, map, startWith } from 'rxjs';
import { reduce } from 'rxjs/internal/operators/reduce';

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html'
})
export class RootComponent {
  constructor() {
    // this.getPromise();
    // this.getObservable();

    // // --------------------------------------
    // this.getPromise().then((data) => {
    //   console.log("Promise - Data", data);
    // }).catch((error) => {
    //   console.error("Promise - Error", error);
    // });

    // const obSub = this.getObservable().subscribe({
    //   next: (data: number) => {
    //     console.log("Observable - Data", data);
    //   },
    //   error: (error: any) => {
    //     console.error("Observable - Error", error);
    //   },
    // });

    // setTimeout(() => {
    //   obSub.unsubscribe();
    // }, 9000);

    // // -------------------------------------- Observables are single-cast
    // const observable = this.getObservable();

    // observable.subscribe({
    //   next: (data: number) => {
    //     console.log("Subscriber 1 - Data", data);
    //   }
    // });

    // observable.subscribe({
    //   next: (data: number) => {
    //     console.log("Subscriber 2 - Data", data);
    //   }
    // });

    // // -------------------------------------- Subjects are multi-cast
    // const subject: any = this.getSubject();

    // subject.subscribe({
    //   next: (data: number) => {
    //     console.log("Subscriber 1, - Data: ", data);
    //   }
    // });

    // subject.subscribe({
    //   next: (data: number) => {
    //     console.log("Subscriber 2, - Data: ", data);
    //   }
    // });

    // // subject.next(1);
    // // subject.complete();
    // subject.error("Some Error");

    //   // -------------------------------------- Subjects As Observables
    //   const obseravble: any = this.getSubjectAsObservable();

    //   obseravble.subscribe({
    //     next: (data: number) => {
    //       console.log("Subscriber 1, - Data: ", data);
    //     }
    //   });

    //   obseravble.subscribe({
    //     next: (data: number) => {
    //       console.log("Subscriber 2, - Data: ", data);
    //     }
    //   });

    // // ----------------------------- Subject Types
    // // const subject: any = this.getSubjectA();
    // // const subject: any = this.getSubjectB();
    // const subject: any = this.getSubjectC();

    // let s1 = subject.subscribe({
    //   next: (data: number) => {
    //     console.log("Subscriber 1, - Data: ", data);
    //   }
    // });

    // let s2: Subscription;

    // setTimeout(() => {
    //   console.log("\nAfter 6 secs, S2 subscribed: ")

    //   s2 = subject.subscribe({
    //     next: (data: number) => {
    //       console.log("Subscriber 2, - Data: ", data);
    //     }
    //   });
    // }, 6100);

    // setTimeout(() => {
    //   s1.unsubscribe();
    //   s2.unsubscribe();
    //   console.log("\nAfter 11 secs, unsubscribed from all subscriptions");
    // }, 11000);

    // ----------------------------- Operators

    // let numObservable = from([10, 20, 31, 43, 50, 67, 70, 80, 89, 90]);

    // // numObservable.subscribe({
    // //   next: (n: number) => {
    // //     console.log(n);
    // //   }
    // // });

    // // numObservable.pipe(filter((n: number) => n % 2 === 0)).subscribe({
    // //   next: (n: number) => {
    // //     console.log(n);
    // //   }
    // // });

    // // numObservable.pipe(map((n: number) => n * 10)).subscribe({
    // //   next: (n: number) => {
    // //     console.log(n);
    // //   }
    // // });

    // // numObservable.pipe(reduce((acc: number, n: number) => acc + n)).subscribe({
    // //   next: (n: number) => {
    // //     console.log(n);
    // //   }
    // // });

    // numObservable.pipe(
    //   filter((n: number) => n % 2 === 0),
    //   map((n: number) => n * 10),
    //   reduce((acc: number, n: number) => acc + n),
    //   delay(4000)
    // ).subscribe({
    //   next: (n: number) => {
    //     console.log(n);
    //   }
    // });

    // ---------------------

    // let ob1 = from([10, 20, 30]);
    // let ob2 = from([40, 50, 60]);

    // concat(ob1, ob2).subscribe({
    //   next: (n: number) => {
    //     console.log(n);
    //   }
    // });

    concat(
      this.delayedMessage("Get Ready!"),
      this.delayedMessage("3"),
      this.delayedMessage("2"),
      this.delayedMessage("1"),
      this.delayedMessage("Go Now!")
    ).subscribe({
      next: (n: string) => console.log(n)
    });
  }

  delayedMessage(message: any, delayTime = 1000) {
    return EMPTY.pipe(startWith(message), delay(delayTime));
  }

  getSubjectC(): ReplaySubject<number> {
    let s = new ReplaySubject<number>();
    let count = 1;

    setInterval(() => {
      s.next(count++);
    }, 2000);

    return s;
  }

  getSubjectB(): BehaviorSubject<number> {
    let s = new BehaviorSubject<number>(0);     // Initial Value - 0
    let count = 1;

    setInterval(() => {
      s.next(count++);
    }, 2000);

    return s;
  }

  getSubjectA(): Subject<number> {
    let s = new Subject<number>();
    let count = 1;

    setInterval(() => {
      s.next(count++);
    }, 2000);

    return s;
  }

  getSubjectAsObservable(): Observable<number> {
    let subject = new Subject<number>();

    setInterval(() => {
      subject.next(Math.random());
    }, 4000);

    return subject.asObservable();
  }

  getSubject(): Subject<number> {
    let subject = new Subject<number>();

    setInterval(() => {
      subject.next(Math.random());
    }, 4000);

    return subject;
  }

  getObservable(): Observable<number> {
    return new Observable((observer: any) => {
      setInterval(() => {
        // console.log("Observable - Set Interval Executed....");
        observer.next(Math.random());
      }, 4000);
    });
  }

  getPromise(): Promise<number> {
    return new Promise((resolve, reject) => {
      setInterval(() => {
        // console.log("Promise - Set Interval Executed....");
        resolve(Math.random());
      }, 4000);
    });
  }
}
