// import { Component, OnInit } from '@angular/core';
// import { authorList } from 'src/app/data/author-list-data';
// import { Author } from 'src/app/models/author.interface';

// @Component({
//     selector: 'app-root',
//     templateUrl: './root.component.html',
//     styleUrls: ['./root.component.css']
// })
// export class RootComponent implements OnInit {
//     list?: Array<Author>;
//     selectedAuthor?: Author;

//     constructor() { }

//     ngOnInit() {
//         this.list = authorList;
//     }

//     selectAuthor(a: Author) {
//         this.selectedAuthor = a;
//     }

//     isSelected(a: Author) {
//         return this.selectedAuthor === a;
//     }
// }

// ---------------------------------------------------- DI
import { Component, Inject, OnInit } from '@angular/core';
import { authorList } from 'src/app/data/author-list-data';
import { Author } from 'src/app/models/author.interface';
import { AUTHORS_TOKEN } from 'src/app/utilities/tokens/di-tokens';

@Component({
    selector: 'app-root',
    templateUrl: './root.component.html',
    styleUrls: ['./root.component.css'],
    providers: [
        // { provide: 'Authors', useValue: authorList }
        { provide: AUTHORS_TOKEN, useValue: authorList }
    ]
})
export class RootComponent implements OnInit {
    list?: Array<Author>;
    selectedAuthor?: Author;

    // constructor(@Inject('Authors') private authors: Array<Author>) { }
    constructor(@Inject(AUTHORS_TOKEN) private authors: Array<Author>) { }

    ngOnInit() {
        this.list = this.authors;
    }

    selectAuthor(a: Author) {
        this.selectedAuthor = a;
    }

    isSelected(a: Author) {
        return this.selectedAuthor === a;
    }
}