import { InjectionToken } from "@angular/core";

export const AUTHORS_TOKEN = new InjectionToken<string>('Authors');
export const AUTHORS_NEW_TOKEN = new InjectionToken<string>('AuthorsNew');