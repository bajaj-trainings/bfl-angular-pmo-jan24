import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription, delay } from 'rxjs';
import { Author } from 'src/app/models/author.interface';
import { AuthorsService } from 'src/app/services/authors.service';
import { PubSubService } from 'src/app/services/pub-sub.service';

@Component({
  selector: 'author-quote',
  templateUrl: './author-quote.component.html',
  styleUrls: ['./author-quote.component.css']
})
export class AuthorQuoteComponent implements OnInit, OnDestroy {
  selectedAuthor?: Author;
  isLoading: boolean = false;
  sac_sub?: Subscription;
  sal_sub?: Subscription;

  constructor(private authorsService: AuthorsService, private pubSubService: PubSubService) { }

  ngOnInit(): void {
    this.sac_sub = this.pubSubService.on("author-selected").pipe(delay(5000)).subscribe({
      next: () => {
        this.selectedAuthor = this.authorsService.SelectedAuthor;
        this.isLoading = false;
      }
    });

    this.sal_sub = this.pubSubService.on("author-selected").subscribe({
      next: () => {
        this.isLoading = true;
      }
    });
  }

  ngOnDestroy(): void {
    this.sac_sub?.unsubscribe();
  }
}
