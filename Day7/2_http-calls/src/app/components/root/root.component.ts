import { Component } from '@angular/core';
import { Subscription } from 'rxjs';
import { Post } from 'src/app/models/post.model';
import { PostService } from 'src/app/services/post.service';

@Component({
    selector: 'app-root',
    templateUrl: './root.component.html',
    styleUrls: ['./root.component.css'],
    providers: [PostService]
})
export class RootComponent {
    message: string = 'Loading Data, please wait...';
    posts?: Array<Post>;
    get_sub?: Subscription;

    constructor(private postService: PostService) {
        this.get_sub = this.postService.getAllPosts().subscribe({
            next: resData => {
                this.posts = resData;
                this.message = '';
            }, error: err => {
                this.message = err;
            }
        });
    }

    deletePost(id: number, e: Event) {
        e.preventDefault();

        this.message = `Deleting a record with id: ${id}...`;

        this.postService.deletePost(id).subscribe({
            next: () => {
                this.posts = this.posts?.filter(p => p.id !== id);
                this.message = `Record with id: ${id} deleted successfully!`;
                setTimeout(() => {
                    this.message = '';
                }, 3000);
            },
            error: (err: string) => {
                this.message = err;
            }
        });
    }
}