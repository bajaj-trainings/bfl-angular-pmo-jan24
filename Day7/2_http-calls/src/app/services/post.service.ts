import { environment } from "src/environments/environment";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Post } from "../models/post.model";
import { Injectable } from "@angular/core";
import { Observable, catchError, delay, retry, throwError } from "rxjs";

@Injectable()
export class PostService {
  url: string = environment.portsApiUrl;

  constructor(private httpClient: HttpClient) { }

  getAllPosts() {
    return this.httpClient.get<Array<Post>>(this.url).pipe(
      delay(3000),
      retry(3),
      catchError(this._handleError('getAllPosts', []))
    );
  }

  deletePost(id: number) {
    const deleteUrl = `${this.url}/${id}`;

    return this.httpClient.delete(deleteUrl).pipe(
      delay(3000),
      retry(3),
      catchError(this._handleError('deletePost'))
    );
  }

  private _handleError<T>(operation = 'operation', result?: T) {
    return (err: HttpErrorResponse): Observable<T> => {
      console.log(`${operation} failed: ${err.message}`);
      switch (err.status) {
        case 403:
        case 404:
          return throwError(() => err.error.message);
        case 500:
          return throwError(() => err.statusText);
        default:
          return throwError(() => "Connection Error, please try again later...");
      }
    }
  }
}
