import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductNotSelectedComponent } from './components/product-not-selected/product-not-selected.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductsRootComponent } from './components/products-root/products-root.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    ProductNotSelectedComponent,
    ProductDetailsComponent,
    ProductsRootComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    ProductNotSelectedComponent,
    ProductDetailsComponent,
    ProductsRootComponent
  ]
})
export class ProductsModule { }
