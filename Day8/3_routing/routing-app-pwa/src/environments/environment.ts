export const environment = {
    production: true,
    usersUrl: 'http://localhost:8000/api/users',
    accountUrl: 'http://localhost:8000/account/getToken',
    productsUrl: 'http://localhost:8000/api/products'
};