class Identity {
    private id: number;

    get Id() {
        return this.id;
    }
}

class Employee {
    private idCard: Identity;

    get IdNumber() {
        return this.idCard.Id;
    }
}

class Project {
    private manager: Employee;

    get ManagerId() {
        return this.manager.IdNumber;
    }
}

var p = new Project();
console.log(p.ManagerId);